package StringConcept;

public class String1 {

	public static void main(String[] args) {
		String s1="Welcome";//string constant pool area
		char[] c= {'s','e','l','e','n','i','u','m'};
		String s2="Welcome";//scpa
		String s3=new String("Welcome");//stored in heap memory
		String s4=new String("Hello World");//stored heap memory
		String s5=new String(c);//stored heap memory
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);
		System.out.println(s5);
		s1.concat("java");
		System.out.println(s1);
		s1=s1.concat("java");
		System.out.println(s1);
	}
}