package StringConcept;

public class String2 {
	
	public static void main(String[] args) {
		
	
	String s1="Welcome";
	String s2="Welcome";
	String s3=new String("Welcome");
	String s4="java";
	boolean a=s1.equals(s2);//true
	boolean b=s1.equals(s3);//true
	boolean c=s1.equals(s4);//false
	boolean d=s2.equals(s3);//true
	System.out.println(a);
	System.out.println(b);
	System.out.println(c);
	System.out.println(d);// equal method check the contents and not the reference
	
	boolean a1=s1==s2;//true
	boolean a2=s1==s3;//false
	boolean a3=s1==s4;//false
	boolean a4=s2==s3;//false
	System.out.println(a1);
	System.out.println(a2);
	System.out.println(a3);
	System.out.println(a4);
}
}