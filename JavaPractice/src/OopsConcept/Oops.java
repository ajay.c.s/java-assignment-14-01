package OopsConcept;

public class Oops extends Inheritiance implements BasicCar {

	@Override
	public void gearChange() {
		System.out.println("Interface method 1");

	}

	@Override
	public void music() {
		System.out.println("Interface method 2");

	}

	public void oopsSample() {
		System.out.println("Method Overloading Method with 0 args");
	}
	
	public void oopsSample(int a, String b) {
		System.out.println("Method Overloading Method with 2 args");
	}
	
	public void oopsSample(String a, int b) {
		System.out.println("Method Overloading Method with different order");
	}
	public void food() {
		
	}
	public static void main(String[] args) {
		Inheritiance p1=new Inheritiance();
		Oops o1= new Oops();
		o1.oopsSample();
		o1.oopsSample(5, "ABC");
		o1.oopsSample("ABC", 5);
		o1.food();
		o1.gearChange();
		o1.music();
}
}
