package Collection;

import java.util.ArrayList;

public class DEmoArrayList {
	
	public static void main(String[] args) {
		
	
	ArrayList<String> list = new ArrayList<>();

	list.add("a");
	list.add("b");
	list.add("c");
	list.add("d");
	list.add("e");
	list.add("null");
	System.out.println(list.size());
	System.out.println(list);
	list.set(1, "B1");
	System.out.println(list);
	list.remove(3);
	System.out.println(list);
	list.remove("e");
	System.out.println(list);
	
}
}