package Collection;

import java.util.ArrayList;

public class ArrayListTest {
	public static void main(String[] args) {
		
		ArrayList <String> list = new ArrayList <>();
		System.out.println(list.size());
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		
		System.out.println(list.size());
		System.out.println(list);
		//replace the second element with B1
		list.set(1, "B1");
		System.out.println(list);
		
		list.remove(3);
		System.out.println(list);
		
		list.remove("e");
		System.out.println(list);
	}

}
