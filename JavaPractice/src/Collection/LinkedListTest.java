package Collection;

import java.util.LinkedList;

public class LinkedListTest {

	public static void main(String[] args) {

		LinkedList <String> list = new LinkedList<String>();
		
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		
		System.out.println(list);
		list.add(1, "a1");
		System.out.println(list);
		list.addFirst("x");
		list.addLast("z");
		System.out.println(list);
		
		list.remove("e");
		System.out.println(list);
	}
}
//a point to b,b points c,c points to d,d points to e
//add a1 after a
//a point to a1,a1 points c,c points to d,d points to e





