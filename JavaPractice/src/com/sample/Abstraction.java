package com.sample;
// an abstract class is the class which consist of both abstract and non abstract method
// Non abstract method is not compulsory it can have all the method as abstract class
// An abstract is a method without the body,it is just declared. (contains the declaration)
//abstract class cannot be instantiated,that is we cannot create object.
//child class can implement all the abstract method present in the parent class (abstract class).
//what happens if a child does not implement those abstract class?



public abstract class Abstraction {
	

	public abstract void write();
	
}

	

	

