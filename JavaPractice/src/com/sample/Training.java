package com.sample;

public class Training {

	int a=0; //instance var
	static int b=5; //static var
	public void sample() {
		int c=20; //local var
		a=a+5;
		System.out.println(a);
	}
	public static void main(String[] args) {
		Training p =new Training();
		Training p1 =new Training();
		
		//a=a+20;//we cannot call instance variable without creating object
		//System.out.println(a);
		b=b-5;
		System.out.println(b); // we can access static variable without using an object
		//c=c*2; //the scope of local variable is only in method
		
		
		
	}
}
