package Abstraction;

public class NoteBook extends Book {
	private void draw() {
		System.out.println("draw");
	}

	public void write() {
		System.out.println("write");

	}

	public void read() {

		System.out.println("read");
	}

	public static void main(String[] args) {
		NoteBook nb = new NoteBook();
		nb.draw();
		nb.write();
		nb.read();
	}
}
