package Set;

import java.util.TreeSet;

public class Treeset {

	public static void main(String[] args) {

		TreeSet<Integer> evenNum = new TreeSet<>();

		evenNum.add(2);
		evenNum.add(4);
		evenNum.add(6);
		System.out.println("TreeSet: " + evenNum);

		TreeSet<Integer> numbers = new TreeSet<>();
		numbers.add(1);

		numbers.addAll(evenNum);
		System.out.println("New TreeSet: " + numbers);
	}
}
